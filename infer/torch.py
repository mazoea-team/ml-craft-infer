# -*- coding: utf-8 -*-
import os
import time
import logging
import numpy as np
import torch
_logger = logging.getLogger('infer.torch')
PROFILE = os.environ.get('MAZ_PROFILE', '') == '1'

thread_cnt = -1
if os.environ.get('TORCH_THREADS', '') != '':
    try:
        thread_cnt = int(os.environ['TORCH_THREADS'])
        torch.set_num_threads(thread_cnt)
    except Exception as e:
        _logger.warning('Cannot set INFER_MP [%s]', repr(e))
_logger.info('INFER_MP=%d', thread_cnt)

if os.environ.get('TORCH_THREADS', '') != '':
    torch.set_num_threads(int(os.environ['TORCH_THREADS']))
from torch.autograd import Variable

from .. import craft_utils, imgproc, torchutil
if os.environ.get('MAZ_MODEL', None) == 'VGG':
    from pycraft.craft_torch_vgg import CRAFT
else:
    from pycraft.craft_torch_resnet import CRAFT


class args_like(dict):
    """
        Args like dictionary accessible directly via attributes.

        Can be used instead of `args`.
    """

    def __init__(self, *args, **kwargs):
        super(args_like, self).__init__(*args, **kwargs)
        self.__dict__ = self


class inferator(object):

    class inf_params(object):
        def __init__(self):
            self.canvas_size = -1
            self.mag_ratio = 1.5
            self.text_threshold = 0.7
            self.link_threshold = 0.4
            self.low_text = 0.4
            self.cpu = False

    def __init__(self, model_path, params=None):
        self.model_path = model_path
        self.net = CRAFT(pretrained=False)
        self.cuda = False
        self._res_dir = False
        self._callback_input = None
        self._callback_result = None
        #
        self.poly = False
        self.params = params or inferator.inf_params()
        #
        self.problems = []

    @property
    def version(self):
        return "cuda:%s:%s [%s, %s] on [%s]" % (
            self.cuda, self.params.cpu, self.params.canvas_size,
            self.params.mag_ratio, os.path.basename(self.model_path),
        )

    @property
    def callback_input(self):
        return self._callback_input

    @callback_input.setter
    def callback_input(self, val):
        self._callback_input = val

    @property
    def callback_result(self):
        return self._callback_result

    @callback_result.setter
    def callback_result(self, val):
        self._callback_result = val

    def _infer(self, image, params, poly, refine_net=None, np_image=False):
        """
            _logger.info("infer/postproc time : {:.3f}/{:.3f}".format(t0, t1))

        :param image:
        :param params:
        :param poly:
        :param refine_net:
        :return:
        """
        self.net.eval()

        if params.cpu:
            set_flush = torch.set_flush_denormal(True)
            pass

        t0 = time.time()

        ratio_h = ratio_w = 1.
        # resize
        if np_image is False:
            img_resized, target_ratio, size_heatmap = imgproc.resize_aspect_ratio(
                image,
                params.canvas_size,
                mag_ratio=params.mag_ratio)
            _logger.debug('Resized to [%s], ratio [%4.2f] with [%s] to canvas [%s] and mag [%s]',
                          img_resized.shape[0:2], target_ratio, image.shape[0:2], params.canvas_size, params.mag_ratio)
            ratio_h = ratio_w = 1. / target_ratio

            if self.callback_input is not None:
                self.callback_input(img_resized)

            # preprocessing
            x = imgproc.normalizeMeanVariance(img_resized)
            x = torch.from_numpy(x).permute(2, 0, 1)  # [h, w, c] to [c, h, w]
            x = Variable(x.unsqueeze(0))  # [c, h, w] to [b, c, h, w]
        else:
            x = image

        if self.cuda:
            x = x.cuda()

        prof = None
        t_mod = time.time()

        # forward pass
        with torch.no_grad():
            if PROFILE:
                import torch.autograd.profiler as profiler
                with profiler.profile(record_shapes=True) as prof:
                    with profiler.record_function("model_inference"):
                        y, feature = self.net(x)
            else:
                y, feature = self.net(x)

        t_mod = time.time() - t_mod

        # make score and link map
        score_text = y[0, :, :, 0].cpu().data.numpy()
        score_link = y[0, :, :, 1].cpu().data.numpy()
        score_orientation = None
        if 3 == y.shape[-1]:
            score_orientation = y[0, :, :, 2].cpu().data.numpy()

        # taken from CRAFT-pytorch
        # refine link
        if refine_net is not None:
            with torch.no_grad():
                y_refiner = refine_net(y, feature)
            score_link = y_refiner[0, :, :, 0].cpu().data.numpy()

        t0 = time.time() - t0
        t1 = time.time()

        # Post-processing
        boxes, polys = craft_utils.getDetBoxes(
            score_text, score_link, params.text_threshold, params.link_threshold, params.low_text, poly)

        # coordinate adjustment
        boxes = craft_utils.adjustResultCoordinates(boxes, ratio_w, ratio_h)
        polys = craft_utils.adjustResultCoordinates(polys, ratio_w, ratio_h)
        for k in range(len(polys)):
            if polys[k] is None:
                polys[k] = boxes[k]

        t1 = time.time() - t1

        if self.callback_result is not None:
            render_img = score_text.copy()
            render_img = np.hstack((render_img, score_link))
            ret_score_text = imgproc.cvt2HeatmapImg(render_img)
            if score_orientation is not None:
                score_orientation = imgproc.angle_heat_map(
                    score_orientation, score_text)
                ret_score_text = np.hstack((ret_score_text, score_orientation))

            self.callback_result(ret_score_text)

        return boxes, polys, (t0, t1, t_mod), prof

    def load_net(self):
        _logger.debug('Loading weights from checkpoint {} on cuda:{}'.format(
            self.model_path, self.cuda))
        if self.cuda:
            sd = torch.load(self.model_path)
        else:
            sd = torch.load(self.model_path, map_location='cpu')
        if 'state_dict' in sd:
            sd = sd['state_dict']
        self.net.load_state_dict(torchutil.copyStateDict(sd, None), strict=False)

        if self.cuda:
            from torch.backends import cudnn
            self.net = self.net.cuda()
            self.net = torch.nn.DataParallel(self.net)
            cudnn.benchmark = False
        else:
            self.net = self.net.cpu()
        _logger.debug('Loaded weights')

    def set_params_testet(self, testset):
        if testset == 'ICDAR2013':
            self.poly = False
            self.params.canvas_size = 980
            self.params.mag_ratio = 1.5
        elif testset == 'ICDAR2015':
            self.poly = False
            self.params.canvas_size = 980
            self.params.mag_ratio = 1.5

    def infer_one(self, image_path, clip_bb):
        image = imgproc.load_image_np(image_path)
        if image is None:
            return image_path, None, [], []

        orig_w_h = image.shape[0:2]
        if clip_bb is not None:
            image, M = imgproc.clip(image, clip_bb)

        t = time.time()
        bboxes, polys, (t0, t1, t_mod), prof = self._infer(image, self.params, self.poly)
        took = time.time() - t

        # adjust
        if clip_bb is not None:
            bboxes += clip_bb[0]
            polys += clip_bb[0]

        _logger.debug(
            'Inferring took [%4.2f] (resize+infer:[%4.2f], postprocess:[%4.2f], model only [%4.2f])', took, t0, t1, t_mod)
        if prof is not None:
            _logger.debug(prof.key_averages().table(
                sort_by="cpu_time_total", row_limit=10))
            _logger.debug(40 * '=')
            _logger.debug(prof.key_averages(group_by_input_shape=True).table(
                sort_by="cpu_time_total", row_limit=10))
            _logger.debug(40 * '=')
            _logger.debug(prof.key_averages().table(
                sort_by="self_cpu_memory_usage", row_limit=10))
            _logger.debug(40 * '=')

        return image_path, image, bboxes, polys

    def infer(self, image_list, result_folder=None, timestamp=None, result_zip=None, perf=False):
        store_results_zip = None
        if result_zip:
            import zipfile
            store_results_zip = zipfile.ZipFile(result_zip, 'w', zipfile.ZIP_DEFLATED)

        self.problems = []
        res = []
        for k, image_path in enumerate(image_list):
            if perf:
                _logger.info("#{:d}/{:d}: {:s}".format(k +
                                                       1, len(image_list), image_path))

            if result_folder is not None:
                self.set_dbg_one(image_path, result_folder, timestamp)

            image_path, image, bboxes, polys = self.infer_one(image_path, None)
            res.append((image_path, image, bboxes, polys))

            try:
                if store_results_zip is not None:
                    res_file = self.save_result(
                        image_path, image[:, :, ::-1], polys, None, result_folder)
                    store_results_zip.write(res_file, arcname=os.path.basename(res_file))
            except Exception as e:
                _logger.exception('Could not save [%s]', image_path)
                self.problems.append(image_path)

        if store_results_zip is not None:
            store_results_zip.close()

        return res

    def save_result(self, image_path, image, polys, clip_bb, output_folder):
        return imgproc.save_infer_result(
            image_path, image[:, :, ::-1], polys, clip_bb, dirname=output_folder)
