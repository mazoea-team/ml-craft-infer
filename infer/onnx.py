# -*- coding: utf-8 -*-
import os
import time
import logging
import numpy as np
_logger = logging.getLogger('infer.onnx')

from .. import craft_utils, imgproc


class inferator(object):

    class inf_params(object):
        def __init__(self):
            self.canvas_size = -1
            self.mag_ratio = 1.5
            self.text_threshold = 0.7
            self.link_threshold = 0.4
            self.low_text = 0.4
            self.cpu = False

    def __init__(self, model_path, params=None):
        self.model_path = model_path
        self.net = None
        self.cuda = False
        self._res_dir = False
        self._callback_input = None
        self._callback_result = None
        #
        self.poly = False
        self.params = params or inferator.inf_params()
        #
        self.problems = []

    @property
    def version(self):
        return "cuda:%s:%s [%s, %s] on [%s]" % (
            self.cuda, self.params.cpu, self.params.canvas_size,
            self.params.mag_ratio, os.path.basename(self.model_path),
        )

    @property
    def callback_input(self):
        return self._callback_input

    @callback_input.setter
    def callback_input(self, val):
        self._callback_input = val

    @property
    def callback_result(self):
        return self._callback_result

    @callback_result.setter
    def callback_result(self, val):
        self._callback_result = val

    def _infer(self, image, params, poly, refine_net=None, np_image=False):
        """
            _logger.info("infer/postproc time : {:.3f}/{:.3f}".format(t0, t1))

        :param image:
        :param params:
        :param poly:
        :param refine_net:
        :return:
        """
        t0 = time.time()

        ratio_h = ratio_w = 1.
        # resize
        if np_image is False:
            img_resized, target_ratio, size_heatmap = imgproc.resize_aspect_ratio(
                image,
                params.canvas_size,
                mag_ratio=params.mag_ratio)
            _logger.debug('Resized to [%s], ratio [%4.2f] with [%s] to canvas [%s] and mag [%s]',
                          img_resized.shape[0:2], target_ratio, image.shape[0:2], params.canvas_size, params.mag_ratio)
            ratio_h = ratio_w = 1. / target_ratio

            if self.callback_input is not None:
                self.callback_input(img_resized)

            img_with_proper_dims = np.zeros((params.canvas_size, params.canvas_size, 3))
            img_with_proper_dims[:img_resized.shape[0], :img_resized.shape[1]] = img_resized

            x = imgproc.normalizeMeanVariance(img_with_proper_dims)
            x = np.moveaxis(x, [0, 1, 2], [1, 2, 0])
            x = np.expand_dims(x, axis=0)

        else:
            x = image

        input_name = self.net.get_inputs()[0].name
        y, feature = self.net.run(None, {input_name: x})

        # make score and link map
        score_text = y[0, :, :, 0]
        score_link = y[0, :, :, 1]

        t0 = time.time() - t0
        t1 = time.time()

        # Post-processing
        boxes, polys = craft_utils.getDetBoxes(
            score_text, score_link, params.text_threshold, params.link_threshold, params.low_text, poly)

        # coordinate adjustment
        boxes = craft_utils.adjustResultCoordinates(boxes, ratio_w, ratio_h)
        polys = craft_utils.adjustResultCoordinates(polys, ratio_w, ratio_h)
        for k in range(len(polys)):
            if polys[k] is None:
                polys[k] = boxes[k]

        t1 = time.time() - t1

        if self.callback_result is not None:
            render_img = score_text.copy()
            render_img = np.hstack((render_img, score_link))
            ret_score_text = imgproc.cvt2HeatmapImg(render_img)
            self.callback_result(ret_score_text)

        return boxes, polys, (t0, t1)

    def load_net(self):
        _logger.debug('Loading weights from checkpoint {} on cuda:{}'.format(
            self.model_path, self.cuda))

        import onnxruntime as rt

        sess_options = rt.SessionOptions()
        # Set graph optimization level, Enables basic and extended optimizations
        sess_options.graph_optimization_level = rt.GraphOptimizationLevel.ORT_DISABLE_ALL
        # sess_options.enable_sequential_execution = True
        sess_options.optimized_model_filepath = self.model_path
        # sess_options.enable_profiling = True
        # sess_options.intra_op_num_threads = 4
        # sess_options.execution_mode = rt.ExecutionMode.ORT_SEQUENTIAL
        # sess_options.execution_mode = rt.ExecutionMode.ORT_PARALLEL
        # sess_options.inter_op_num_threads = 8

        # EP_list = ['CPUExecutionProvider']
        # model = onnx.load(onnx_file_path)
        # from quantize import quantize, QuantizationMode
        # quantized_model = quantize(model,
        #                            quantization_mode=QuantizationMode.IntegerOps,
        #                            symmetric_weight=True,
        #                            force_fusions=True)

        self.net = rt.InferenceSession(self.model_path, sess_options)
        # print(session.get_providers())

        _logger.debug('Loaded weights')

    def set_params_testet(self, testset):
        if testset == 'ICDAR2013':
            self.poly = False
            self.params.canvas_size = 980
            self.params.mag_ratio = 1.5
        elif testset == 'ICDAR2015':
            self.poly = False
            self.params.canvas_size = 980
            self.params.mag_ratio = 1.5

    def infer_one(self, image_path, clip_bb):
        image = imgproc.load_image_np(image_path)
        if image is None:
            return image_path, None, [], []

        orig_w_h = image.shape[0:2]
        if clip_bb is not None:
            image, M = imgproc.clip(image, clip_bb)

        t = time.time()
        bboxes, polys, _1 = self._infer(image, self.params, self.poly)
        took = time.time() - t

        # adjust
        if clip_bb is not None:
            bboxes += clip_bb[0]
            polys += clip_bb[0]

        _logger.debug('Inferring took [%4.2f]', took)
        return image_path, image, bboxes, polys

    def infer(self, image_list, result_folder=None, timestamp=None, result_zip=None, perf=False):
        store_results_zip = None
        if result_zip:
            import zipfile
            store_results_zip = zipfile.ZipFile(result_zip, 'w', zipfile.ZIP_DEFLATED)

        self.problems = []
        res = []
        for k, image_path in enumerate(image_list):
            if perf:
                _logger.info("#{:d}/{:d}: {:s}".format(k +
                                                       1, len(image_list), image_path))

            if result_folder is not None:
                self.set_dbg_one(image_path, result_folder, timestamp)

            image_path, image, bboxes, polys = self.infer_one(image_path, None)
            res.append((image_path, image, bboxes, polys))

            try:
                if store_results_zip is not None:
                    res_file = self.save_result(
                        image_path, image[:, :, ::-1], polys, None, result_folder)
                    store_results_zip.write(res_file, arcname=os.path.basename(res_file))
            except Exception as e:
                _logger.exception('Could not save [%s]', image_path)
                self.problems.append(image_path)

        if store_results_zip is not None:
            store_results_zip.close()

        return res

    def save_result(self, image_path, image, polys, clip_bb, output_folder):
        return imgproc.save_infer_result(
            image_path, image[:, :, ::-1], polys, clip_bb, dirname=output_folder)
