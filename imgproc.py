# -*- coding: utf-8 -*-
import os
import io
import random
import logging
import numpy as np
import cv2
# from skimage import io
_logger = logging.getLogger('imgproc')


def show(img, title, wait=True):
    cv2.imshow(title, img)
    if wait:
        cv2.waitKey()


def load_image(img_file):
    # ORIGINAL - seems equal - img_orig = io.imread(img_file)  # RGB order
    img = cv2.imread(img_file, cv2.IMREAD_COLOR)
    try:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    except Exception as e:
        _logger.info('Invalid image [%s]\n[%s]', img_file, repr(e))
        return None
    if img.shape[0] == 2:
        img = img[0]
    if len(img.shape) == 2:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    if img.shape[2] == 4:
        img = img[:, :, :3]
    return img


def load_image_np(img_file):
    return np.array(load_image(img_file))


def normalizeMeanVariance(in_img, mean=(0.485, 0.456, 0.406), variance=(0.229, 0.224, 0.225)):
    # should be RGB order
    img = in_img.copy().astype(np.float32)

    img -= np.array([mean[0] * 255.0, mean[1] * 255.0,
                     mean[2] * 255.0], dtype=np.float32)
    img /= np.array([variance[0] * 255.0, variance[1] * 255.0,
                     variance[2] * 255.0], dtype=np.float32)
    return img


def denormalizeMeanVariance(in_img, mean=(0.485, 0.456, 0.406), variance=(0.229, 0.224, 0.225)):
    # should be RGB order
    img = in_img.copy()
    img *= variance
    img += mean
    img *= 255.0
    img = np.clip(img, 0, 255).astype(np.uint8)
    return img


def resize_aspect_ratio(img, exp_size, interpolation=None, mag_ratio=1):
    height, width, channel = img.shape

    # magnify image size
    target_size = mag_ratio * max(height, width)

    # set original image size
    if target_size > exp_size:
        target_size = exp_size

    ratio = float(target_size) / max(height, width)

    target_h, target_w = int(height * ratio), int(width * ratio)

    if interpolation is None:
        interpolation = cv2.INTER_LINEAR if 1. < ratio else cv2.INTER_AREA
    proc = cv2.resize(img, (target_w, target_h), interpolation=interpolation)

    # make canvas and paste image
    target_h32, target_w32 = target_h, target_w
    if target_h % 32 != 0:
        target_h32 = target_h + (32 - target_h % 32)
    if target_w % 32 != 0:
        target_w32 = target_w + (32 - target_w % 32)
    resized = np.zeros((target_h32, target_w32, channel), dtype=np.float32)
    resized[0:target_h, 0:target_w, :] = proc
    target_h, target_w = target_h32, target_w32

    size_heatmap = (int(target_w / 2), int(target_h / 2))

    return resized, ratio, size_heatmap


def clip(img, poly4):
    w = int(np.linalg.norm(poly4[0] - poly4[1]))
    h = int(np.linalg.norm(poly4[0] - poly4[3]))
    M = cv2.getPerspectiveTransform(np.float32(poly4),
                                    np.float32(np.array([[0, 0], [w, 0], [w, h], [0, h]])))
    warped = cv2.warpPerspective(img, M, (w, h))
    return warped, M


def cvt2HeatmapImg(img):
    img = (np.clip(img, 0, 1) * 255).astype(np.uint8)
    img = cv2.applyColorMap(img, cv2.COLORMAP_JET)
    return img


def angle_heat_map(img, conf_map=None):
    heat_img = cvt2HeatmapImg(img)

    if conf_map is not None:
        heat_img[np.where(conf_map < 0.6)] = [255, 255, 255]

    return heat_img


# =====================================

def get_w_h(bb):
    xmin = bb[:, 0].min()
    xmax = bb[:, 0].max()
    ymin = bb[:, 1].min()
    ymax = bb[:, 1].max()
    width = xmax - xmin
    height = ymax - ymin
    return width, height


def get_min_max(bb):
    xmin = bb[:, 0].min()
    xmax = bb[:, 0].max()
    ymin = bb[:, 1].min()
    ymax = bb[:, 1].max()
    return xmin, xmax, ymin, ymax


def get_min_max_arr(bb):
    xmin = bb[:, :, 0].min()
    xmax = bb[:, :, 0].max()
    ymin = bb[:, :, 1].min()
    ymax = bb[:, :, 1].max()
    return xmin, xmax, ymin, ymax


def get_min_maxes_w_h(bb):
    xmin, xmax, ymin, ymax = get_min_max(bb)
    width = max(1, int(xmax) - int(xmin))
    height = max(1, int(ymax) - int(ymin))
    return xmin, xmax, ymin, ymax, width, height


def resize(img, mag):
    new_dim = (int(img.shape[1] * mag), int(img.shape[0] * mag))
    interpolation = cv2.INTER_LINEAR if 1. < mag else cv2.INTER_AREA
    return cv2.resize(img, new_dim, interpolation=interpolation)


def to_poly(arr):
    return np.array([
        [arr[0], arr[1]],
        [arr[2], arr[3]],
        [arr[4], arr[5]],
        [arr[6], arr[7]],
    ], np.int32)


def bb4_to_poly(arr):
    xmin, ymin, xmax, ymax = arr
    return np.array([
        [xmin, ymin],
        [xmax, ymin],
        [xmax, ymax],
        [xmin, ymax],
    ], np.int32)


def show_bboxes_poly(image, bboxes, thickness=3):
    for i in range(len(bboxes)):
        # BGR
        c = (
            random.randint(50, 205),
            random.randint(50, 205),
            0
        )
        pts = np.reshape(np.int32(bboxes[i]), (-1, 1, 2))
        cv2.polylines(image, [pts], True, c, thickness=thickness)
    return image


def get_bboxes(cnt, stats, min_area):
    rects = []
    for i in range(1, cnt):
        x, y = stats[i, cv2.CC_STAT_LEFT], stats[i, cv2.CC_STAT_TOP]
        w, h = stats[i, cv2.CC_STAT_WIDTH], stats[i, cv2.CC_STAT_HEIGHT]
        if w * h < min_area:
            continue
        rects.append(to_poly([x, y, x + w, y, x + w, y + h, x, y + h]))
    rects.sort(key=lambda x: x[0][0])
    return rects


def cc_get_letters_bboxes(word_image, debug=0):
    gray = word_image if len(word_image.shape) != 3 else cv2.cvtColor(
        word_image, cv2.COLOR_RGB2GRAY)
    ret, word_binary_bin = cv2.threshold(
        gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    if 1 < debug:
        show(word_binary_bin, 'word_binary_bin', False)
    # for letters
    nb_letter_components, _, letter_stats, _ = cv2.connectedComponentsWithStats(
        word_binary_bin, connectivity=8)

    img_h, img_w = word_image.shape[0:2]
    letter_rects = get_bboxes(nb_letter_components, letter_stats, img_h / 5 * 3)

    if 1 < debug:
        word_image_letter_dbg = show_bboxes_poly(
            word_image.copy(), letter_rects)
        mag = 4
        show(resize(word_image_letter_dbg, mag), 'word_image_letter_dbg', False)

    return letter_rects


def cc_get_word_bboxes(word_image, k, debug=0):
    gray = word_image if len(word_image.shape) != 3 else cv2.cvtColor(
        word_image, cv2.COLOR_RGB2GRAY)

    ret, word_binary_bin = cv2.threshold(
        gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # for text
    kernel = np.ones((3, k), np.uint8)
    word_binary_bin_closed = cv2.morphologyEx(
        word_binary_bin, cv2.MORPH_CLOSE, kernel, iterations=1)
    if 1 < debug:
        show(word_binary_bin_closed, 'word_binary_bin_closed', False)
    nb_word_components, _, word_stats, _ = cv2.connectedComponentsWithStats(
        word_binary_bin_closed, connectivity=8)

    img_h, img_w = word_image.shape[0:2]
    word_rects = get_bboxes(nb_word_components, word_stats, img_h / 5 * 3)

    if 1 < debug:
        word_image_word_dbg = show_bboxes_poly(word_image.copy(), word_rects)
        mag = 4
        show(resize(word_image_word_dbg, mag), 'word_image_word_dbg', False)

    return word_rects


def bboxes_x_overlap(bboxes, min_overlap):
    for i in range(0, len(bboxes) - 1):
        l1 = np.min(bboxes[i][:, 0])
        r1 = np.max(bboxes[i][:, 0])
        l2 = np.min(bboxes[i + 1][:, 0])
        r2 = np.max(bboxes[i + 1][:, 0])
        if r1 < l2 or r2 < l1:
            continue
        # more than 75% overlap
        perc_overlap = (
            100. * (min(r1, r2) - max(l1, l2))) / min(r2 - l2, r1 - l1)
        if min_overlap <= perc_overlap:
            return True
    return False


def bboxes_y_overlap(bboxes, min_overlap):
    for i in range(0, len(bboxes) - 1):
        l1 = np.min(bboxes[i][:, 1])
        r1 = np.max(bboxes[i][:, 1])
        l2 = np.min(bboxes[i + 1][:, 1])
        r2 = np.max(bboxes[i + 1][:, 1])
        if r1 < l2 or r2 < l1:
            continue
        # more than 75% overlap
        perc_overlap = (
            100. * (min(r1, r2) - max(l1, l2))) / min(r2 - l2, r1 - l1)
        if min_overlap <= perc_overlap:
            return True
    return False


def proper_bb(bb):
    tl, tr, br, bl = bb[0:4]
    # top x
    if tr[0] < tl[0] + 1:
        return False
    if br[0] < bl[0] + 1:
        return False

    if bl[1] < tl[1] + 1:
        return False
    if br[1] < tr[1] + 1:
        return False

    return True


def create_gt_heatmaps(region_scores, affinity_scores, confidence_mask):
    target_gaussian_heatmap_color = cvt2HeatmapImg(region_scores / 255)
    target_gaussian_affinity_heatmap_color = cvt2HeatmapImg(
        affinity_scores / 255)
    confidence_mask_gray = cvt2HeatmapImg(confidence_mask)
    vline = np.zeros(
        shape=(target_gaussian_heatmap_color.shape[0], 5, 3), dtype=np.float32)
    return target_gaussian_heatmap_color, target_gaussian_affinity_heatmap_color, confidence_mask_gray, vline


def crop_image_by_bbox(image, box, text_len=-1):
    w = (int)(np.linalg.norm(box[0] - box[1]))
    h = (int)(np.linalg.norm(box[0] - box[3]))
    width = w
    height = h
    rotate = False

    # at least 25 pixes wide to rotate or NET will fail
    w_to_64 = (w * (64.0 / h))
    rot_w_to_64 = (h * (64.0 / w))
    min_w = 20
    if 2 < text_len and (w * 2.5 < h or w_to_64 < min_w < rot_w_to_64 or (w < h * 0.75 < h < 100)):
        rotate = True

    if rotate:
        width = h
        height = w
        M = cv2.getPerspectiveTransform(np.float32(box),
                                        np.float32(np.array([[width, 0], [width, height], [0, height], [0, 0]])))
    else:
        M = cv2.getPerspectiveTransform(np.float32(box),
                                        np.float32(np.array([[0, 0], [width, 0], [width, height], [0, height]])))
        rotate = False

    warped = cv2.warpPerspective(image, M, (width, height))
    return warped, M, rotate


def save_infer_result(img_file, img, boxes, clip_bb=None, dirname=None, texts=None):
    """
        Save text detection
    """
    img = np.array(img)
    if dirname is None:
        import tempfile
        dirname = tempfile.gettempdir()

    # make result file list
    filename, file_ext = os.path.splitext(os.path.basename(img_file))

    # result directory
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

    # txt bboxes
    res_file_bboxes = os.path.join(dirname, "res_" + filename + '.bboxes.txt')
    with io.open(res_file_bboxes, 'w+', newline='\n', encoding='utf-8') as f:
        for i, box in enumerate(boxes):
            poly = np.array(box).astype(np.int32)
            xmin, xmax, ymin, ymax = get_min_max(poly)
            strResult = u','.join([str(xmin), str(ymin), str(xmax), str(ymax)])
            f.write(strResult + u'\n')

    # txt polys
    res_file_polys = os.path.join(dirname, "res_" + filename + '.polys.txt')
    with io.open(res_file_polys, 'w+', newline='\n', encoding='utf-8') as f:
        for i, box in enumerate(boxes):
            poly = np.array(box).astype(np.int32).reshape((-1))
            strResult = u','.join([str(p) for p in poly])
            f.write(strResult + u'\n')

    # image
    res_img_file = os.path.join(dirname, "res_" + filename + '.jpg')
    for i, box in enumerate(boxes):
        if clip_bb is not None:
            box -= clip_bb[0]
        poly = np.array(box).astype(np.int32).reshape((-1))
        poly = poly.reshape(-1, 2)
        cv2.polylines(img, [poly.reshape((-1, 1, 2))],
                      True, color=(0, 0, 255), thickness=2)
        if texts is not None:
            font = cv2.FONT_HERSHEY_SIMPLEX
            font_scale = 0.5
            cv2.putText(img, "{}".format(texts[i]), (poly[0][0] + 1, poly[0][1] + 1), font, font_scale, (0, 0, 0),
                        thickness=1)
            cv2.putText(img, "{}".format(texts[i]), tuple(
                poly[0]), font, font_scale, (0, 255, 255), thickness=1)

    # Save result image
    cv2.imwrite(res_img_file, img)
    return res_file_bboxes, res_file_polys
