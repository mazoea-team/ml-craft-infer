# -*- coding: utf-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F
from . import torchutil

RESNET = 'resnet18'
# include 3rd channel for orientation map
ORIENTATION = True

if RESNET == 'resnet50':
    from .basenet.resnet50 import resnet50 as resnet
elif RESNET == 'resnet18':
    from .basenet.resnet18 import resnet18 as resnet


class double_conv(nn.Module):
    def __init__(self, in_ch, mid_ch, out_ch):
        super(double_conv, self).__init__()
        self.conv = nn.Sequential(
            # see https://pytorch.org/tutorials/recipes/recipes/tuning_guide.html
            # Disable bias for convolutions directly followed by a batch norm
            nn.Conv2d(in_ch + mid_ch, mid_ch, kernel_size=1, bias=False),
            nn.BatchNorm2d(mid_ch),
            nn.ReLU(inplace=True),
            nn.Conv2d(mid_ch, out_ch, kernel_size=3, padding=1, bias=False),
            nn.BatchNorm2d(out_ch),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.conv(x)
        return x


class CRAFT(nn.Module):
    def __init__(self, pretrained=True, freeze=False):
        super(CRAFT, self).__init__()

        """ Base network """
        self.basenet = resnet(pretrained, freeze)

        """ U network """
        if RESNET == 'resnet50':
            self.upconv1 = double_conv(2048, 1024, 512)
            self.upconv2 = double_conv(512, 512, 256)
            self.upconv3 = double_conv(256, 256, 64)
            self.upconv4 = double_conv(64, 64, 32)
        elif RESNET == 'resnet18':
            self.upconv1 = double_conv(512, 256, 256)
            self.upconv2 = double_conv(256, 128, 128)
            self.upconv3 = double_conv(128, 64, 64)
            self.upconv4 = double_conv(64, 64, 32)

        num_class = 3 if ORIENTATION else 2
        self.conv_cls = nn.Sequential(
            nn.Conv2d(32, 32, kernel_size=3, padding=1), nn.ReLU(inplace=True),
            nn.Conv2d(32, 32, kernel_size=3, padding=1), nn.ReLU(inplace=True),
            nn.Conv2d(32, 16, kernel_size=3, padding=1), nn.ReLU(inplace=True),
            nn.Conv2d(16, 16, kernel_size=1), nn.ReLU(inplace=True),
            nn.Conv2d(16, num_class, kernel_size=1),
        )

        torchutil.init_weights(self.upconv1.modules())
        torchutil.init_weights(self.upconv2.modules())
        torchutil.init_weights(self.upconv3.modules())
        torchutil.init_weights(self.upconv4.modules())
        torchutil.init_weights(self.conv_cls.modules())

    def forward(self, x):
        """ Base network """
        sources = self.basenet(x)
        # RETURNs vgg_outputs(h_fc7, h_relu5_3, h_relu4_3, h_relu3_2, h_relu2_2)

        y = sources[0]
        y = F.interpolate(y, size=sources[1].size()[2:],
                          mode='bilinear', align_corners=False)

        """ U network """
        y = torch.cat([y, sources[1]], dim=1)
        y = self.upconv1(y)

        y = F.interpolate(y, size=sources[2].size()[2:],
                          mode='bilinear', align_corners=False)
        y = torch.cat([y, sources[2]], dim=1)
        y = self.upconv2(y)

        y = F.interpolate(y, size=sources[3].size()[2:],
                          mode='bilinear', align_corners=False)
        y = torch.cat([y, sources[3]], dim=1)
        y = self.upconv3(y)

        y = F.interpolate(y, size=sources[4].size()[2:],
                          mode='bilinear', align_corners=False)
        y = torch.cat([y, sources[4]], dim=1)
        feature = self.upconv4(y)

        y = self.conv_cls(feature)

        return y.permute(0, 2, 3, 1), feature


if __name__ == '__main__':
    model = CRAFT(pretrained=True).cuda()
    output, _ = model(torch.randn(1, 3, 768, 768).cuda())
    print(output.shape)
