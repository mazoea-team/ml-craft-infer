import os
import torch
import torch.nn as nn

from collections import namedtuple
from torchvision.models import vgg16_bn as vgg16_bn_raw
from .. import torchutil

weights_folder = os.path.join(os.path.dirname(__file__), '../../pretrain')


class vgg16_bn(torch.nn.Module):
    def __init__(self, pretrained=True, freeze=False):
        super(vgg16_bn, self).__init__()
        # vgg_pretrained_features = models.vgg16_bn(pretrained=pretrained).features
        vgg_pretrained_features = vgg16_bn_raw(pretrained=False)
        if pretrained:
            vgg_pretrained_features.load_state_dict(
                torchutil.copyStateDict(torch.load(os.path.join(weights_folder, 'vgg16_bn-6c64b313.pth')), None))
        vgg_pretrained_features = vgg_pretrained_features.features
        self.slice1 = torch.nn.Sequential()
        self.slice2 = torch.nn.Sequential()
        self.slice3 = torch.nn.Sequential()
        self.slice4 = torch.nn.Sequential()
        self.slice5 = torch.nn.Sequential()
        for x in range(13):  # conv2_2
            self.slice1.add_module(str(x), vgg_pretrained_features[x])
        for x in range(13, 20):  # conv3_3
            self.slice2.add_module(str(x), vgg_pretrained_features[x])
        for x in range(20, 30):  # conv4_3
            self.slice3.add_module(str(x), vgg_pretrained_features[x])
        for x in range(30, 40):  # conv5_3
            self.slice4.add_module(str(x), vgg_pretrained_features[x])

        # fc6, fc7 without atrous conv
        self.slice5 = torch.nn.Sequential(
            nn.MaxPool2d(kernel_size=3, stride=1, padding=1),
            nn.Conv2d(512, 1024, kernel_size=3, padding=6, dilation=6),
            nn.Conv2d(1024, 1024, kernel_size=1)
        )

        if not pretrained:
            torchutil.init_weights(self.slice1.modules())
            torchutil.init_weights(self.slice2.modules())
            torchutil.init_weights(self.slice3.modules())
            torchutil.init_weights(self.slice4.modules())

        # no pretrained model for fc6 and fc7
        torchutil.init_weights(self.slice5.modules())

        if freeze:
            for param in self.slice1.parameters():  # only first conv
                param.requires_grad = False

    def forward(self, X):
        h = self.slice1(X)
        h_relu2_2 = h
        h = self.slice2(h)
        h_relu3_2 = h
        h = self.slice3(h)
        h_relu4_3 = h
        h = self.slice4(h)
        h_relu5_3 = h
        h = self.slice5(h)
        h_fc7 = h
        vgg_outputs = namedtuple(
            "VggOutputs", ['fc7', 'relu5_3', 'relu4_3', 'relu3_2', 'relu2_2'])
        out = vgg_outputs(h_fc7, h_relu5_3, h_relu4_3, h_relu3_2, h_relu2_2)
        return out
