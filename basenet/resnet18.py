import os
import torch
import torch.nn as nn

from collections import namedtuple
from torchvision.models import resnet18 as resnet18_raw
from .. import torchutil

weights_folder = os.path.join(os.path.dirname(__file__), '../../pretrain')


# ====================================

class resnet18(torch.nn.Module):
    def __init__(self, pretrained=True, freeze=False):
        super(resnet18, self).__init__()
        # vgg_pretrained_features = models.vgg16_bn(pretrained=pretrained).features
        self.m = resnet18_raw(pretrained=False)
        if pretrained:
            self.m.load_state_dict(
                torchutil.copyStateDict(torch.load(os.path.join(weights_folder, 'resnet18-5c106cde.pth')), None))

        if freeze:
            for param in self.m.layer1.parameters():
                param.requires_grad = False

    def forward(self, x):
        x = self.m.conv1(x)
        x = self.m.bn1(x)
        x_0 = x
        x = self.m.relu(x)
        x = self.m.maxpool(x)

        x = self.m.layer1(x)
        x_1 = x
        x = self.m.layer2(x)
        x_2 = x
        x = self.m.layer3(x)
        x_3 = x
        x = self.m.layer4(x)
        x_4 = x

        resnet_outputs = namedtuple(
            "ResnetOutputs", ['wtf4', 'wtf3', 'wtf2', 'wtf1', 'wtf0'])
        out = resnet_outputs(x_4, x_3, x_2, x_1, x_0)
        return out
